﻿/*
 * Created by SharpDevelop.
 * User: hhazan01
 * Date: 07/07/2010
 * Time: 19:41
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace NeuronArch
{
	
	//----------------------------------------------------------------
	public class McCulloch_Pitts : NeuronArch.Unit
	{
		//----------------------------------------------------------------
		//					init ver
		//----------------------------------------------------------------
		double initV;
		public double V;
		double therashold;
	    double initTherashold;
		public double Output;
		double spike;
		
		public McCulloch_Pitts(ref globalParam Param)
		{
			this.initTherashold=1; // init value the Nueron shold rest this!
			this.reset(ref Param);
		}
		//----------------------------------------------------------------
		
		public void reset(ref globalParam Param)
		{
			this.initV =  Param.initV;
			this.V = this.initV;
			this.therashold = this.initTherashold;
			this.Output  = 0 ;
			this.spike = Param.Neuron_Spike;
		}
		//----------------------------------------------------------------
		
		public void setThreshold(double NewT){
        	this.initTherashold = NewT;
        }
		
		
		public double step(double InternalInput, double ExternalIntput)
		{
			this.Output = 0;
			
			if (InternalInput+ExternalIntput>=this.therashold)
				this.Output = this.spike;
			this.V = this.Output;
			
			return (this.Output);
		}
		//----------------------------------------------------------------
		
		public double returnV()
		{
			return (this.V);
		}
		//----------------------------------------------------------------

		public bool returnOutput()
		{
			return (this.Output > this.therashold);
		}
		//----------------------------------------------------------------
		
		public void setV(double NewV)
		{
			this.V = NewV;
		}
		//----------------------------------------------------------------
	}
}

