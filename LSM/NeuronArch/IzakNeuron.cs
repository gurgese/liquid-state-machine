
using System;

namespace NeuronArch
{
    // Iimplementation of Izhikevich Model
    public class IzakNeuron : NeuronArch.Unit
    {
        //----------------------------------------------------------------
        //					init ver
        //----------------------------------------------------------------
        double initV;
        public double V;
        double a,b,c,d,U;
        double therashold;
        double inittherashold;
        public double Output;
        
        //Random rnd = new Random();
        //----------------------------------------------------------------

        public IzakNeuron(ref globalParam Param)
        {
            this.reset(ref Param);
        }

        //----------------------------------------------------------------
        public void reset(ref globalParam Param)
        {	            
            this.V = Param.initV;
            this.a = 0.02 ;//* this.rnd.NextDouble();
            this.b = 0.2;
            this.c = -65;
            this.d = 2 ;
            this.U = this.b * V;
            this.initV = this.c;
            this.Output = 0;
            this.inittherashold = Param.Neuron_Threshold;
            this.therashold = this.inittherashold;

        }

        //----------------------------------------------------------------
        public double step(double InternalInput, double ExternalIntput)
        {
            //IMPORTENT!!!!!! THE INPUT IS TOO STRONG!!
        	//ExternalIntput=ExternalIntput/10;
        	//InternalInput=InternalInput/10;
            
            this.Output = 0;
            
            if (this.V >= this.therashold){
                this.Output = this.V;
                this.V = this.c;
                this.U += this.d;
            }

            this.V += this.V*(0.04*this.V + 5) + 140 - this.U + (InternalInput + ExternalIntput);
            this.U += this.a * (this.b*this.V - this.U);

            return (this.Output);
        }

        //----------------------------------------------------------------
        public double returnV()
        {
            return (this.V);
        }

        //----------------------------------------------------------------
        public bool returnOutput()
        {
            return (this.Output > this.therashold);
        }
        //----------------------------------------------------------------
        public void setV(double NewV)
        {
            this.V = NewV;
        }
        //----------------------------------------------------------------
        
        public void setThreshold(double NewT)
        {
            this.inittherashold = NewT;
        }
        //----------------------------------------------------------------
        
    }
}