
using System;


namespace NeuronArch
{

	public class LIFNeuron : NeuronArch.Unit
	{
		//----------------------------------------------------------------
		//                    init ver
		//----------------------------------------------------------------
		double initV;
		public double V;
		double initDecay;
		double decay;
		double decayFactor;
		double refractory;
		double refractoryV;
		double therashold;
		double inittherashold;
		public double Output;
		//----------------------------------------------------------------

		public LIFNeuron(ref globalParam Param)
		{
			this.reset(ref Param);
		}

		//----------------------------------------------------------------
		public void reset(ref globalParam Param)
		{
			this.initV = Param.initV;
			this.V = initV;
			this.initDecay = Param.decayFactor+1;
			this.decay = initDecay;
			this.decayFactor = Param.decayFactor+1;
			this.refractory = 1.3;
			this.refractoryV = -130.0;

			this.therashold = this.inittherashold;
			this.Output = 0;

		}

		//----------------------------------------------------------------
		public double step(double InternalInput, double ExternalIntput)
		{
			this.Output = 0;

			if (this.V >= this.therashold){
				this.Output = this.V;
				this.V = this.refractoryV ;
				this.decay = this.initDecay;
			}else if (this.V >= this.initV){
				if ((InternalInput > 0) || (ExternalIntput > 0)){
					this.V = this.V + InternalInput + ExternalIntput;
					this.decay = this.initDecay;
				}else{
					this.V = (this.V * this.decay);
					this.decay = (this.decay * this.decayFactor);
					if (this.decay > 1.9) this.decay = 1.9;
					
					// if there is no input the V is becoume minus BIG BIG number almost infinity
					if (this.V < this.initV) this.V = this.initV;
				}
				if (this.therashold < inittherashold) this.therashold = inittherashold;
			}else if (this.V < this.initV){
				this.V = (this.V / this.refractory);
				this.decay = this.initDecay;
			}
			return (this.Output);
		}

		//----------------------------------------------------------------
		public double returnV()
		{
			return (this.V);
		}

		//----------------------------------------------------------------
		public bool returnOutput()
		{
			return (this.Output > this.therashold);
		}

		//----------------------------------------------------------------
		public void setV(double NewV)
		{
			this.V = NewV;
		}
		//----------------------------------------------------------------
		
		public void setThreshold(double NewT)
		{
			this.inittherashold = NewT;
		}
		//----------------------------------------------------------------
		
	}
}
