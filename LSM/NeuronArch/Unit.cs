using System;

namespace NeuronArch
{
    /// <summary>
    /// Description of Interface1.
    /// </summary>
    public interface Unit
    {
        void reset(ref globalParam Param);
        double step(double InternalInput,double ExternalIntput);
        double returnV();
        bool returnOutput();
        void setV(double NewV);
        void setThreshold(double NewT);
    }
    
}
