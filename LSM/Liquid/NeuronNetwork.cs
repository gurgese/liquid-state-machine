﻿using System;
using Neurons;
using System.IO;
using Utils_Functions;
using Inputs;

namespace NeuronNetwork
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class Network
	{
		//----------------------------------------------------------------
		//                    init ver
		//----------------------------------------------------------------
		Neuron[] InputNeurons;
		Neuron[] OutputNeurons;
		Neuron[] ListOfAllNeurons;
		bool[,] ConnectivityMatrix;
		Random rnd;
		//Random rnd = new Random(System.Environment.TickCount);
		randomIntArr rndA;
		//----------------------------------------------------------------

		
		public Network(ref int NetOutputSize,ref globalParam Param,ref double[] ConnectionDistribution_Input,ref double[] ConnectionDistribution_Output,ref int inputNsize)
		{
			long tempSeed = (System.DateTime.UtcNow.Ticks);
			this.rnd = new Random(Convert.ToInt32(tempSeed%int.MaxValue));
			int NetSize=Param.Number_Of_Neurons;
			ConnectivityMatrix = new bool[NetSize, NetSize];
			bool[] InputConnectivityMatrix = new bool[NetSize];
			double[,] weight = new double[NetSize, NetSize];
			int[,] delay = new int[NetSize, NetSize];
			int[] posiORneg = new int[NetSize];
			tempSeed = (System.DateTime.UtcNow.Ticks);
			rndA = new randomIntArr(Convert.ToInt32(tempSeed%int.MaxValue));
			

			// Neuron can be Only Negative(2) OR Positive(1) in its weights!!
			int[] temp = new int[(int)Math.Round(NetSize*Param.LSM_Percent_Of_Negative_Weights)];
			rndA.select(0,NetSize,ref temp);
			for (int i = 0 ;i<temp.Length ; i++ ) {	posiORneg[temp[i]]=1; }
			for (int i = 0; i < NetSize; i++){	posiORneg[i]++;}
			temp = null;

			//----------------------------------------------------------------------------------------------------------------------
			
			switch(Param.Liquid_Architecture){
				case 0:
					this.RandomConnection(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 1:
					this.PowerLawConnection_Method1(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 2:
					this.FeedForwardWithHubs(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 3:
					this.Maass(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 4:
					this.GroupsPowerLaw(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 5:
					this.UncorrelatedScale_Free(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 6:
					this.UncorrelatedScale_Free_Powerlaw(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 7:
					this.PowerLawSelections(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 8:
					this.TwoWayPowerLaw(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
				case 9:
					this.TwoWayLinearDescent(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
					break;
			}
			Console.WriteLine("finish building connections");
			//----------------------------------------------------------------------------------------------------------------------
			// create Neuron List
			this.ListOfAllNeurons = new Neuron[NetSize];
			for (int i=0 ;i<NetSize;i++ ) {
				this.ListOfAllNeurons[i] = new Neuron(Param.Neuron_Model, i,ref Param);
				this.ListOfAllNeurons[i].posiORneg=posiORneg[i];
			}
			
			// make/creat the connection between neurons
			for (int i = 0; i < NetSize; i++){
				Neuron Source = this.ListOfAllNeurons[i].returnRef();
				for (int j = 0; j < NetSize; j++)
				{
					if (ConnectivityMatrix[i, j] == true)
					{
						Neuron Target = this.ListOfAllNeurons[j].returnRef();
						int wightIndex=Target.addNueronToInputList(ref Source, weight[i, j], delay[i, j]);
						Source.addNueronToOutputList(ref Target,wightIndex,ref Param);
					}
				}
			}
			
			
			// set the Threshold of neuron depending on Modle and Input Size
			double groupA=0,groupB=0;
			for (int i = 0 ; i < this.ListOfAllNeurons.Length ; i++ ){
				int inputNumber = this.ListOfAllNeurons[i].inputFromNeuronID.Length;
				
				if (Param.Proportional_Threshold==0){
					if (Param.Neuron_Model==4){
						groupA=Param.Neuron_Threshold;
						groupB=Param.Neuron_Threshold;
					}else{
						groupA=Param.Neuron_Threshold;
						groupB=Param.Neuron_Threshold;
					}
				}else if (Param.Proportional_Threshold==1){
					if (Param.Neuron_Model==4){
						groupA=Param.Neuron_Threshold;
						groupB=(Param.Neuron_Threshold*(inputNumber*Param.Neuron_Threshold_Proportion));
						if (groupB<Param.Neuron_Threshold) groupB = Param.Neuron_Threshold;
					}else{
						groupA=Param.Neuron_Threshold;
						groupB=(Param.Neuron_Threshold*(inputNumber*Param.Neuron_Threshold_Proportion));
						if (groupB<Param.Neuron_Threshold) groupB = Param.Neuron_Threshold;
					}
				}else if (Param.Proportional_Threshold==2){
					if (Param.Neuron_Model==4){
						groupA=1;
						groupB=inputNumber*Param.Neuron_Threshold;
					}else{
						groupA=Param.Neuron_Threshold;
						groupB=(Param.Neuron_Threshold*(inputNumber*Param.Neuron_Threshold_Proportion));
						if (groupB<Param.Neuron_Threshold) groupB = Param.Neuron_Threshold;
					}
				}
				
				if (inputNumber<=Param.Group_size_Min)
					this.ListOfAllNeurons[i].setThreshold(groupA);
				else
					this.ListOfAllNeurons[i].setThreshold(groupB);
			}
			
			
			inputNsize=(int)Math.Round(NetSize*Param.LSM_Input_Percent_Connectivity);
			// count the input / output neurons
			for (int j = 0; j < NetSize; j++){
				InputConnectivityMatrix[j] = false;
				for (int i = 0; i < NetSize; i++){
					if (ConnectivityMatrix[j, i] == true){
						ConnectionDistribution_Output[j]++;
						ConnectionDistribution_Input[i]++;
					}
				}
			}
			int inputCounter;
			if (Param.Liquid_Readout_Units==0){
				// Random chosen input neurons
				inputCounter = 0;
				int MaassNetSize = 1;
				for (int i = 0 ; i < Param.Maass_column.Length ; i++ )
					MaassNetSize*=Param.Maass_column[i];
				
				while (inputCounter<inputNsize) {
					int inputCandidat = rnd.Next(0,MaassNetSize);
					if (InputConnectivityMatrix[inputCandidat]==true) continue;
					InputConnectivityMatrix[inputCandidat] = true;
					inputCounter++;
				}
			}else if (Param.Liquid_Readout_Units==1){
//				//The Input nueron must have Minimum input to him and more then zero output from him.
//				//also the input nuerons going to be clastering (one near the other)
//				//------------------------------------------------
//				inputCounter=0;
//				int maxOutput=Param.Group_interconnections[0],miniInput=1;
//				while(inputCounter<inputNsize){
//					maxOutput++;
//					if (maxOutput>NetSize){
//						maxOutput=Param.Group_interconnections[0];
//						miniInput++;
//					}
//					for (int i = 0 ; i < ConnectionDistribution_Input.Length ; i++ ) {
//						if ((InputConnectivityMatrix[i] == false)&&(inputCounter<=inputNsize)&&
//						    (ConnectionDistribution_Output[i]>=Param.Group_interconnections[0])&&(ConnectionDistribution_Output[i]<=maxOutput)&&
//						    (ConnectionDistribution_Input[i]==miniInput)) {
//							InputConnectivityMatrix[i] = true;
//							inputCounter++;
//						}
//					}
//				}
//				//===============================================
				inputCounter=0;
				double maxOutput,miniInput;
				Matrix_Arithmetic matrix = new Matrix_Arithmetic();
				miniInput = matrix.Min(ConnectionDistribution_Output);
				maxOutput = 1;
				while(inputCounter<inputNsize){
					if (maxOutput>=NetSize){
						maxOutput=1;
						miniInput++;
					}
					if (miniInput>=NetSize){ miniInput = 0;}
					for (int i = 0 ; i < ConnectionDistribution_Input.Length ; i++ ) {
						if ((InputConnectivityMatrix[i] == false)&&(inputCounter<inputNsize)&&
						    (ConnectionDistribution_Output[i]>0)&&(ConnectionDistribution_Output[i]<=maxOutput)&&
						    (ConnectionDistribution_Input[i]<=miniInput)) {
							InputConnectivityMatrix[i] = true;
							inputCounter++;
						}
					}
					maxOutput++;
				}
			}

			// Creat External Input and Output for Readout Neuron List
			int InputConnection = 0;
			for (int i = 0; i < NetSize; i++) if (InputConnectivityMatrix[i]) InputConnection++;
			NetOutputSize = NetSize - InputConnection;
			this.InputNeurons = new Neuron[InputConnection];
			this.OutputNeurons = new Neuron[NetOutputSize];
			inputCounter = 0;
			int OutputCounter = 0;
			for (int i = 0; i < NetSize; i++)
			{
				if (InputConnectivityMatrix[i]) {   // creat the Input list of the network
					this.InputNeurons[inputCounter] = this.ListOfAllNeurons[i].returnRef();
					inputCounter++;
				}else{ // creat the Output list of the network
					this.OutputNeurons[OutputCounter] = this.ListOfAllNeurons[i].returnRef();
					OutputCounter++;
				}
			}
			
			System.Console.WriteLine("Number of Inputs Neurons : {0}",inputCounter);
			System.Console.WriteLine("Number of Neurons - Inputs : {0}",OutputCounter);
			
		} // End constractor
		//----------------------------------------------------------------
		
		public void TimeStep(int expirament,ref DamageLSM.Input inputVec, ref double[,] output,ref double[,] inputNeurons, ref double[,] Activity,ref globalParam Param)
		{
			/// <summary>
			/// Tests in Iterations :
			/// 1. checking the activity of given a vector two but in the exect time of the expirament 2
			/// 2. checking the activity of give Test Vector one wait a lot of iterations and then give vector two
			/// 3. ater given vector 3 checking the liquid activity after 3/4 of the runing time
			/// </summary>
			
			
			if ( expirament==0 ) // expirament 1
			{
				double[][] input = new double[1][];
				input[0] = inputVec.returnInput(expirament);
				int ptr=0;
				for (int runningTime = 0; runningTime < Param.Activity_Test_Running_Time ; runningTime++)
				{
					if ((runningTime>=(Param.Activity_Test_Greace_Time))&&(ptr < input.Length )) {
						this.InputToTheNet(new double[]{Param.Neuron_Spike});
						Activity[1,runningTime]++;
						ptr++;
					}else{
						this.InputToTheNet(new double[]{0});
					}
					for (int i = 0; i < this.ListOfAllNeurons.Length; i++){
						this.ListOfAllNeurons[i].step(runningTime,ref Param);
						this.ListOfAllNeurons[i].decInputQ();
						//this.ListOfAllNeurons[i].STDP();
						//this.ListOfAllNeurons[i].LTP_D(runningTime);
					}
					for (int i = 0; i < this.OutputNeurons.Length; i++){
						output[i, runningTime] = this.OutputNeurons[i].returnV();
						if ( output[i, runningTime]>=Param.Neuron_Threshold) { Activity[0,runningTime]++;}
					}
					for (int i = 0; i < this.InputNeurons.Length; i++){
						inputNeurons[i, runningTime] = this.InputNeurons[i].returnV();
					}
				}
				
			}
			else if ( expirament == 1 ) // expirament 2
			{
				{
					double[][] input = new double[1][];
					input[0] = inputVec.returnInput(expirament);
					int ptr=0;
					int grace_Time = Param.Activity_Test_Greace_Time+inputVec.returnInput(0).Length+Param.Activity_Test_Time_Between_Inputs ;
					for (int runningTime = 0; runningTime < Param.Activity_Test_Running_Time ; runningTime++)
					{
						if ((runningTime>=grace_Time)&&(ptr < input.Length )) {
							this.InputToTheNet(new double[]{Param.Neuron_Spike});
							Activity[1,runningTime]++;
							ptr++;
						}else{
							this.InputToTheNet(new double[]{0});
						}
						for (int i = 0; i < this.ListOfAllNeurons.Length; i++){
							this.ListOfAllNeurons[i].step(runningTime,ref Param);
							this.ListOfAllNeurons[i].decInputQ();
							//this.ListOfAllNeurons[i].STDP();
							//this.ListOfAllNeurons[i].LTP_D(runningTime);
						}
						for (int i = 0; i < this.OutputNeurons.Length; i++){
							output[i, runningTime] = this.OutputNeurons[i].returnV();
							if ( output[i, runningTime]>=Param.Neuron_Threshold) { Activity[0,runningTime]++;}
						}
						for (int i = 0; i < this.InputNeurons.Length; i++){
							inputNeurons[i, runningTime] = this.InputNeurons[i].returnV();
						}
					}
				}
			}
			else if ( expirament ==2 ) // expirament 3
			{
				double[][] input = new double[2][];
				input[0] = inputVec.returnInput(0);
				input[1] = inputVec.returnInput(1);
				int inputCounter=0,ptr=0,sec=Param.Activity_Test_Greace_Time+Param.Activity_Test_Time_Between_Inputs+input[0].Length;
				
				for (int runningTime = 0; runningTime < Param.Activity_Test_Running_Time ; runningTime++)
				{
					if ( inputCounter==0) {
						if ((runningTime >= Param.Activity_Test_Greace_Time)&&(ptr < input[inputCounter].Length)) {
							this.InputToTheNet(new double[]{Param.Neuron_Spike});
							Activity[1,runningTime]++;
							ptr++;
						}else{
							this.InputToTheNet(new double[]{0});
						}
					}else if (inputCounter==1) {
						if (ptr < (input[inputCounter].Length)) {
							this.InputToTheNet(new double[]{Param.Neuron_Spike});
							Activity[1,runningTime]++;
							ptr++;
						}else{
							this.InputToTheNet(new double[]{0});
						}
					}
					
					if ( runningTime+1==sec) {ptr=0; inputCounter++;}
					
					for (int i = 0; i < this.ListOfAllNeurons.Length; i++){
						this.ListOfAllNeurons[i].step(runningTime,ref Param);
						this.ListOfAllNeurons[i].decInputQ();
						//this.ListOfAllNeurons[i].STDP();
						//this.ListOfAllNeurons[i].LTP_D(runningTime);
					}
					for (int i = 0; i < this.OutputNeurons.Length; i++){
						output[i, runningTime] = this.OutputNeurons[i].returnV();
						if ( output[i, runningTime]>=Param.Neuron_Threshold) { Activity[0,runningTime]++;}
					}
					for (int i = 0; i < this.InputNeurons.Length; i++){
						inputNeurons[i, runningTime] = this.InputNeurons[i].returnV();
					}
				}
			}
		}//----------------------------------------------------------------
		
		public void run_on_vector(ref double[][] input, ref double[,] output,ref double[,] inputNeurons, int Time, int NorGorD, double percent,ref globalParam Param)
		{  // NorGorD = 0-Generalization Output, 1-Normal, 2-Noisy Nuerons, 3-Dead Nuerons
			
			// create interference array
			int times_of_Interference = (int) Math.Round(Time * percent);
			int[] Input_Interference;
			int Interference_Counter = 0;
			if ( times_of_Interference==0) {
				Input_Interference = new int[1];
				Input_Interference[0] = Time+1;
			}else {
				Input_Interference = new int[times_of_Interference];
				rndA.select(0,Time, ref Input_Interference);
				Array.Sort(Input_Interference);
			}
			//
			
			int input_counter=-1,flag=0,silence_or_repeted=Param.silence_or_repeted_Input_between_ratio;
			double[] input_streem = new double[input.Length];
			
			for (int runningTime = 0; runningTime < Time; runningTime++)
			{
				if ((runningTime % Param.LSM_Ratio_Of_Input_Interval[1]) == 0) {input_counter++;flag=1;}
				if (flag==1){
					for (int size = 0 ; size < input_streem.Length ; size++ )
						input_streem[size] = input[size][input_counter % input[size].Length];
					flag = silence_or_repeted;
				}else{
					for (int size = 0 ; size < input_streem.Length ; size++ )
						input_streem[size] = 0;
					flag=0;
				}
				switch (NorGorD) {
					case 0:		//Generalization Test
						if (Input_Interference[Interference_Counter]==input_counter) {
							for (int size = 0 ; size < input_streem.Length ; size++ ) {
								if (input_streem[size] == 1) input_streem[size] = 0;
								else input_streem[size] = 1;
							}
						}
						this.InputToTheNet(input_streem);
						break;
						
					case 1: // Normal Mode
						this.InputToTheNet(input_streem);
						break;
						
					case 2: // Noise Generator
						//if (rnd.NextDouble() <= percent){
						this.InputToTheNet(input_streem);
						spikeGenerator(percent);
						//}
						break;
						
					case 3: // Dead Neurons
						//if (rnd.NextDouble() <= percent){
						this.InputToTheNet(input_streem);
						Damage(percent);
						//}
						break;
						
					case 4: // Combain Damage
						this.InputToTheNet(input_streem);
						if (rnd.NextDouble() <= 0.5){  Damage(percent);
						}else{ spikeGenerator(percent);	}
						break;
						
				}
				if (Param.Liquid_Update_Sync_Or_ASync == 0 ){ // Neurons update in Sync ways
//					//	option 1: for completeness fot Async way to show that its thesame but with less performance
//					int[] neuronOrder = new int[this.ListOfAllNeurons.Length];
//					rndA.select(0,this.ListOfAllNeurons.Length,ref neuronOrder);
//					for (int i = 0; i < neuronOrder.Length; i++){
//						this.ListOfAllNeurons[neuronOrder[i]].step(runningTime,ref Param);
//						this.ListOfAllNeurons[neuronOrder[i]].ExternalIntput = 0;
//						//this.ListOfAllNeurons[i].STDP();
//						//this.ListOfAllNeurons[i].LTP_D(runningTime);
//					}
//					for (int i = 0; i < ListOfAllNeurons.Length; i++) this.ListOfAllNeurons[i].decInputQ();
					
					// option 2: better for performance
					for (int i = 0 ; i < this.ListOfAllNeurons.Length ; i++){
						this.ListOfAllNeurons[i].step(runningTime,ref Param);
						this.ListOfAllNeurons[i].decInputQ();
						this.ListOfAllNeurons[i].ExternalIntput = 0;
						//this.ListOfAllNeurons[i].STDP();
						//this.ListOfAllNeurons[i].LTP_D(runningTime);
					}
					
				}else{  // Neurons update in Async ways
					int[] neuronOrder = new int[this.ListOfAllNeurons.Length];
					rndA.select(0,this.ListOfAllNeurons.Length,ref neuronOrder);
					for (int i = 0; i < neuronOrder.Length; i++){
						this.ListOfAllNeurons[neuronOrder[i]].step(runningTime,ref Param);
						this.ListOfAllNeurons[neuronOrder[i]].decInputQ();
						this.ListOfAllNeurons[neuronOrder[i]].ExternalIntput = 0;
						//this.ListOfAllNeurons[neuronOrder[i]].STDP();
						//this.ListOfAllNeurons[neuronOrder[i]].LTP_D(runningTime);
					}
					
				}

				for (int i = 0; i < this.OutputNeurons.Length; i++){
					output[i, runningTime] = this.OutputNeurons[i].returnV();
				}
				for (int i = 0; i < this.InputNeurons.Length; i++){
					inputNeurons[i, runningTime] = this.InputNeurons[i].returnV();
				}
				
//				if (runningTime%100)
//					for (int i = 0; i < neuronOrder.Length; i++){
//					this.ListOfAllNeurons[i].avgCounter++;
//					this.ListOfAllNeurons[i].setThreshold();
//					this.ListOfAllNeurons[i].firing_count = 0 ;
//				}
				
				
				
			}
		}//----------------------------------------------------------------
		
		public void InputToTheNet(double[] input)
		{
			if (input.Length>this.InputNeurons.Length)
			{
				Console.WriteLine("ERROR : INPUT IS BIGGER THEN THE NUMBER OF INPUT UNITS IN THE LIQUID!!!!");
			}else{
				int input_group=this.InputNeurons.Length/input.Length;
				for (int i = 0; i < input.Length; i++)
				{
					for (int j = input_group * i; j < input_group * (i+1) ; j++)
					{
						this.InputNeurons[j].ExternalIntput = input[i];
					}
				}
				int lastIdx = input.Length-1;
				for (int i = input_group*input.Length ; i < this.InputNeurons.Length ; i++){
					this.InputNeurons[i].ExternalIntput = input[lastIdx];
				}
			}
		}//----------------------------------------------------------------
		
		public void reset(ref globalParam Param)
		{
			for (int i = 0; i < this.ListOfAllNeurons.Length; i++){
				this.ListOfAllNeurons[i].reset(ref Param);
			}
		}//----------------------------------------------------------------
		
		public void spikeGenerator(double percent)
		{
			for (int i = 0; i < this.ListOfAllNeurons.Length; i++){
				if (rnd.NextDouble() <= percent){ this.ListOfAllNeurons[i].mode = 2; }
			}
		}//----------------------------------------------------------------
		
		public void Damage(double percent)
		{
			for (int i = 0; i < this.ListOfAllNeurons.Length; i++){
				if (rnd.NextDouble() <= percent){ this.ListOfAllNeurons[i].mode = 3; }
			}
		}//----------------------------------------------------------------
		
		public void DOTFile()
		{
			TextWriter tw1 = new StreamWriter("DOT network.txt");
			tw1.WriteLine("digraph \"Network Connectivity\"{");
			for(int y = 0 ; y < ConnectivityMatrix.GetLength(0) ; y++){
				int flag = 0;
				for ( int x = 0 ; x < ConnectivityMatrix.GetLength(1) ; x++ ) {
					if ( ConnectivityMatrix[x,y]==true ) {
						if ( flag ==0) { tw1.Write("	"+(1+y).ToString()+" -> "+(1+x).ToString()); flag++; }
						else tw1.Write(" -> "+(1+x).ToString());
					}
				}
				tw1.WriteLine(";");
			}
			tw1.WriteLine("}");
			tw1.Close();
		}
		//----------------------------------------------------------------
		
		public void PrintNetwork()
		{
			// Print the network
			int NetSizeX = this.ConnectivityMatrix.GetLength(0);
			for (int i = 0; i < NetSizeX; i++){
				int NetSizeY = this.ConnectivityMatrix.GetLength(1);
				for (int j = 0; j < NetSizeY; j++){
					if (ConnectivityMatrix[i, j] == true){System.Console.Write("1");}
					else {System.Console.Write("0");}
				}
				System.Console.WriteLine("  ");
			}
		}
		//----------------------------------------------------------------
		
		public void RandomConnection(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                             int NetSize, int[] posiORneg, ref globalParam Param)
		{
			// Random Connections
			// create : Connection, weight and delay between the neurons
			int TotalConnection =0;
			while (TotalConnection < Param.Connections) {
				for (int x = 0 ; x< NetSize ; x++ ) {
					for (int y = 0 ; y < NetSize ; y++ ) {
						if ((rnd.NextDouble()<=Param.GeneralConnectivity)&&(ConnectivityMatrix[x, y] == false)) {
							ConnectivityMatrix[x, y] = true;
							TotalConnection++;
							if (posiORneg[x]==1) {
								do{
									weight[x,y]=Param.LSM_Min_Init_Weight_PosN+(rnd.NextDouble()*Param.LSM_Max_Init_Weight_PosN);
								}while ((weight[x,y]>=Param.LSM_Min_Init_Weight_PosN)&&(weight[x,y]<=Param.LSM_Max_Init_Weight_PosN));
								delay[x,y]=rnd.Next(Param.Neuron_Min_Delay,Param.Neuron_Max_Delay);
							}else{
								do{
									weight[x,y]=-1*(Param.LSM_Min_Init_Weight_NegN+(rnd.NextDouble()*Param.LSM_Max_Init_Weight_NegN));
								}while ((weight[x,y]>=Param.LSM_Min_Init_Weight_NegN)&&(weight[x,y]<=Param.LSM_Max_Init_Weight_NegN));
								delay[x,y]=rnd.Next(Param.Neuron_Min_Delay,Param.Neuron_Max_Delay);
							}
						}
					}
				}
			}
		}//----------------------------------------------------------------------------------------------------------------------
		
		public void PowerLawConnection_Method1(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                                       int NetSize,int[] posiORneg, ref globalParam Param){
			// stat the minimul Group connections
			int Min_Connection = Param.Min_Neuron_Connection;
			int Group_size = Param.Group_size;
			int Netsize = ConnectivityMatrix.GetLength(0)-1;
			for (int x = 0 ; x< Netsize ; x++ ) {
				int group_start = (x/Group_size) * Group_size , group_end = group_start + Group_size ;
				if (group_end>Netsize) group_end = Netsize;
				int[] candidates = new int[Min_Connection];
				rndA.dselect(group_start,group_end,x,ref candidates);
				for (int i = 0 ; i < Min_Connection ; i++) { ConnectivityMatrix[x,candidates[i]]=true; }
			}
			
			// stat the random sellection acording to powerlow
			int[] connectionProbability = new int[ConnectivityMatrix.GetLength(0)];
			for ( int i = 0 ; i<connectionProbability.Length ; i++ ) { connectionProbability[i]=i;	}
			int[] outputNodes,inputNodes,listOfoutputNodes;
			rndA.random_power_low_sellect_Mathod1(Min_Connection,connectionProbability,Param.Connections*10,Param.IncreaseSelectedChanceBy,out outputNodes,out inputNodes);
			int counter = 0;
			listOfoutputNodes = new int[outputNodes.LongLength];
			rndA.select(0,outputNodes.Length, ref listOfoutputNodes);
			rndA.shuffle(ref listOfoutputNodes);

			for (int candidant = 0 ; candidant < inputNodes.Length ; candidant++){
				for ( int times = 0 ; times < inputNodes[candidant] ; times++ ) {
					int node = outputNodes[listOfoutputNodes[counter]];
					/*
					 *   I know that here there is a mistake, if there is already connection between
					 * 	 candidant ---> node  (candidant to node) the cennection will be "mark" as true
					 * 	 again, if this mistake will be corrected the results will be worse
					 */
					//if (node==candidant) counter--;
					while (node==candidant) { node= outputNodes[listOfoutputNodes[rnd.Next(0,outputNodes.Length)]];}
					counter++;
					ConnectivityMatrix[node,candidant]=true;
				}
			}
			
			// check if there is a group that dont have ANY output to the rest of the net
			int[] island_group_Out_Connection = new int[0];
			int flag=0;
			for (int x = 0 ; x< ConnectivityMatrix.GetLength(0) ; x++ ) {
				int group_start = (x/Group_size) * Group_size , group_end = group_start + Group_size ;
				if ( x%Group_size==0){
					if ((flag<Param.Min_Group_Connections)&&(x>0)){
						Array.Resize(ref island_group_Out_Connection,island_group_Out_Connection.Length+1);
						island_group_Out_Connection[island_group_Out_Connection.Length-1]=(x-1)/Group_size;
					}else	flag=0;
					if (x==ConnectivityMatrix.GetLength(0)) continue;
				}
				for (int y = 0 ; y < ConnectivityMatrix.GetLength(1) ; y++ ) {
					if ((y >= group_start)&&(y < group_end)) { continue;}
					if (ConnectivityMatrix[x,y]==true) { flag++; }
				}
			}
			for(int i = 0 ; i < island_group_Out_Connection.Length ; i++){
				int group_start = island_group_Out_Connection[i] * Group_size , group_end = group_start + Group_size ;
				int x=0,y=0;
				while (x==y) {
					x=rnd.Next(group_start,group_end);
					y=(int)outputNodes[listOfoutputNodes[rnd.Next(0,outputNodes.Length)]];
				}
				ConnectivityMatrix[x,y]=true;
			}
			
			// check if there is a group that dont have ANY input from the rest of the net
			int[] island_group_In_Connection = new int[0];
			flag=0;
			for (int x = 0 ; x< ConnectivityMatrix.GetLength(1) ; x++ ) {
				int group_start = (x/Group_size) * Group_size , group_end = group_start + Group_size ;
				if ( x%Group_size==0){
					if ((flag<Param.Min_Group_Connections)&&(x>0)){
						Array.Resize(ref island_group_In_Connection,island_group_In_Connection.Length+1);
						island_group_In_Connection[island_group_In_Connection.Length-1]=(x-1)/Group_size;
					}else	flag=0;
					if (x==ConnectivityMatrix.GetLength(1)) continue;
				}
				for (int y = 0 ; y < ConnectivityMatrix.GetLength(0) ; y++ ) {
					if ((y >= group_start)&&(y < group_end)) { continue;}
					if (ConnectivityMatrix[y,x]==true) { flag++; }
				}
			}
			for(int i = 0 ; i < island_group_In_Connection.Length ; i++){
				int group_start = island_group_In_Connection[i] * Group_size , group_end = group_start + Group_size ;
				int x=0,y=0;
				while (x==y) {
					x=rnd.Next(group_start,group_end);
					y=(int)outputNodes[listOfoutputNodes[rnd.Next(0,outputNodes.Length)]];
				}
				ConnectivityMatrix[y,x]=true;
			}
			
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
			
			outputNodes = new int[1];
			inputNodes = new int[1];
			outputNodes = null;
			inputNodes = null;
		}//----------------------------------------------------------------------------------------------------------------------
		
		public void finishNetworkRewiring(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                                  int NetSize,int[] posiORneg, ref globalParam Param)
		{
			//------------------------- finish part ----------------------------------
			// create : Connection, weight and delay between the neurons
			for (int x = 0 ; x < NetSize ; x++ ) {
				for (int y = 0 ; y < NetSize ; y++ ) {
					if (ConnectivityMatrix[x,y]==true){
						if (posiORneg[x]==1) {
							do{
								weight[x,y]=Param.LSM_Min_Init_Weight_PosN+(rnd.NextDouble()*Param.LSM_Max_Init_Weight_PosN);
							}while ((weight[x,y]>=Param.LSM_Min_Init_Weight_PosN)&&(weight[x,y]<=Param.LSM_Max_Init_Weight_PosN));
							delay[x,y]=rnd.Next(Param.Neuron_Min_Delay,Param.Neuron_Max_Delay);
						}else{
							do{
								weight[x,y]=-1*(Param.LSM_Min_Init_Weight_NegN+(rnd.NextDouble()*Param.LSM_Max_Init_Weight_NegN));
							}while ((weight[x,y]>=Param.LSM_Min_Init_Weight_NegN)&&(weight[x,y]<=Param.LSM_Max_Init_Weight_NegN));
							delay[x,y]=rnd.Next(Param.Neuron_Min_Delay,Param.Neuron_Max_Delay);
						}
					}
				}
			}
		}
		
		
		public void FeedForwardWithHubs(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                                int NetSize,int[] posiORneg, ref globalParam Param)
		{
			int TotalConnections=0;
			// stat the minimul Group connections
			int[] Group_size = new int[0]{};
			int Group_size_Max=Param.Group_size_Max+1,
			Group_size_Min=Param.Group_size_Min,
			Group_sum=0,
			Min_InterConnections = Param.Group_interconnections[0],
			Max_InterConnections = 0;
			
			Matrix_Arithmetic Utils = new Matrix_Arithmetic();
			do{
				int candidateGroup = rnd.Next(Group_size_Min,Group_size_Max);
				if ((Group_sum+Group_size_Max+1)>=ConnectivityMatrix.GetLength(0))
					candidateGroup = (ConnectivityMatrix.GetLength(0) - Group_sum);
				Group_sum += candidateGroup;
				Utils.AddCell(ref Group_size,candidateGroup);
			}while (Group_sum<ConnectivityMatrix.GetLength(0));
			
			int endGroup = Group_size[0]-1,startGroup=0,counter = 0 ;
			for (int x = 0 ; x < ConnectivityMatrix.GetLength(0) ; x++ ) {
				if ((x == endGroup) && (counter+1<Group_size.Length)) {
					counter++;
					startGroup = endGroup;
					endGroup += Group_size[counter];
				}
				int[] candidates;
				if (Param.Group_interconnections.Length==2){
					if (Param.Group_interconnections[1]>=Group_size[counter])
						Max_InterConnections = Group_size[counter]-1;
					else
						Max_InterConnections = Param.Group_interconnections[1];
				}else
					Max_InterConnections = Group_size[counter]-1;
				if (Max_InterConnections < Min_InterConnections)
					candidates = new int[rnd.Next(Max_InterConnections,Min_InterConnections+1)];
				else
					candidates = new int[rnd.Next(Min_InterConnections,Max_InterConnections+1)];
				rndA.dselect(startGroup,endGroup,x,ref candidates);
				for (int i = 0 ; i < candidates.Length ; i++ ) {
					this.ConnectivityMatrix[x,candidates[i]]=true;
					TotalConnections++;
				}
			}
			//-----------------------------------------------------------------------------------------
			// creating Group Connection Matix with number of connection from each group
			int[] hubsGroup = new int[Group_size.Length/4];
			rndA.select(0,Group_size.Length,ref hubsGroup);
			int hubsGroupSize=0;
			for (int i = 0 ; i < hubsGroup.Length ; i++ ) hubsGroupSize+=Group_size[hubsGroup[i]];
			int[] hubNeuronNumbers = new int[hubsGroupSize+1];
			int groupCounter = 0;
			endGroup = Group_size[0]-1; startGroup=0; counter = 0 ;
			for (int x = 0 ; x < NetSize ; x++ ) {
				if ((x == endGroup) && (counter+1<Group_size.Length)) {
					counter++;
					startGroup = endGroup;
					endGroup += Group_size[counter];
				}
				int flag=0;
				for (int i = 0 ; i < hubsGroup.Length ; i++ )
					if (hubsGroup[i]==counter) flag=1;
				if (flag ==1 ) {hubNeuronNumbers[groupCounter] = x; groupCounter++;}
			}
			
//			// Create Random Connection between the Hubs Groups
//			for (int i = 0 ; i <hubsGroup.Length ; i++ ) {
//				int shift=0;
//				for (int t = 0 ; t < hubsGroup[i] ; t++ ) { shift+=Group_size[t];	}
//				int candidateGroup;
//				do{	candidateGroup = rnd.Next(0,hubsGroup.Length);
//				}while(candidateGroup==i);
//				int targrtShift=0;
//				for (int t = 0 ; t < candidateGroup ; t++ ) { targrtShift+=Group_size[t];	}
//				int source,target;
//				do{
//				source = rnd.Next(0,Group_size[hubsGroup[i]]) + shift;
//				target = rnd.Next(0,Group_size[hubsGroup[candidateGroup]]) + targrtShift;
//				}while (this.ConnectivityMatrix[source,target]==true);
//				this.ConnectivityMatrix[source,target]=true;
//				TotalConnections++;
//			}
			
			// Creat the rest of the net group
			int[] restOfTheNet = new int[Group_size.Length-hubsGroup.Length];
			counter=0;
			for (int i= 0 ; i < Group_size.Length ; i++ ){
				int flag =0;
				for (int t = 0 ; t < hubsGroup.Length ; t++ )
					if (i==hubsGroup[t]) flag=1;
				if (flag==0) {restOfTheNet[counter] = i; counter++;}
			}
//			// Greate Random Connection between the Group of rest of the net
//			for (int i = 0 ; i <restOfTheNet.Length ; i++ ) {
//				int shiftSource=0;
//				for (int t = 0 ; t < restOfTheNet[i] ; t++ ) { shiftSource+=Group_size[t];	}
//				int candidateGroup;
//				do{	candidateGroup = rnd.Next(0,restOfTheNet.Length);
//				}while(candidateGroup==i);
//				int targrtShift=0;
//				for (int t = 0 ; t < candidateGroup ; t++ ) { targrtShift+=Group_size[t];	}
//				int source,target;
//				do{
//					source = rnd.Next(0,Group_size[restOfTheNet[i]]) + shiftSource;
//					target = rnd.Next(0,Group_size[restOfTheNet[candidateGroup]]) + targrtShift;
//				}while (this.ConnectivityMatrix[source,target]==true);
//				this.ConnectivityMatrix[source,target]=true;
//				TotalConnections++;
//			}
			
			
			
			//choose random representative from group restOftheNet to be conect to representative in group hubGroup
			for (int time = 0 ; time < Param.Connections-TotalConnections ; time++) {
				for(int r = 0 ; r < restOfTheNet.Length ; r++){
					int groupBegin = 0;
					for (int i = 0 ; i < restOfTheNet[r] ; i++) groupBegin+=Group_size[i];
					int flag=0,neuronNumber=0,targetCandidate=0;counter=0;
					while ((flag==0)&&(counter<Group_size[restOfTheNet[r]])) {
						int candidate = rnd.Next(0,Group_size[restOfTheNet[r]]);
						neuronNumber = groupBegin+candidate;
						flag = 1;
						targetCandidate = hubNeuronNumbers[rnd.Next(0,hubNeuronNumbers.Length)];
						if (this.ConnectivityMatrix[neuronNumber,targetCandidate]==true) flag=0;
						if (neuronNumber==targetCandidate) flag=0;
						counter++;
					}
					if (flag==1) {
						this.ConnectivityMatrix[neuronNumber,targetCandidate]=true;
						TotalConnections++;
					}
				}
			}
			
			//search for the minimon input connection in the all net of the network and connect some of the
			//hubNetwork with thr minimum out put connection
			int[] miniInputNeuros;//,miniOutputNeurons;
			miniInputNeuros = new int[0];
			Matrix_Arithmetic ArrayManimulation = new Matrix_Arithmetic();
			for (int min = 0 ; min < Min_InterConnections ; min++) {
				for (int neu = 0 ; neu < NetSize ; neu++) {
					int neuSumIn=0;
					for (int i = 0 ; i < NetSize ; i++ )
						if (this.ConnectivityMatrix[i,neu]==true) neuSumIn++;
					
					if (neuSumIn==min) ArrayManimulation.AddCell(ref miniInputNeuros,neu);
				}
			}
			
			// find the minimum output in the hubNet
			int minimumOutput = NetSize;
			for (int neu = 0 ; neu < hubNeuronNumbers.Length ; neu++ ) {
				int neuSumOut = 0;
				for (int i = 0 ; i < NetSize ; i++)
					if (this.ConnectivityMatrix[hubNeuronNumbers[neu],i]==true) neuSumOut++;
				if (minimumOutput>neuSumOut) minimumOutput = neuSumOut;
			}

			int[] hubNeuronNumbersMinimumOutput = new int[0];
			for( ;  hubNeuronNumbersMinimumOutput.Length < (miniInputNeuros.Length)/2 ; minimumOutput++){
				//if (hubNeuronNumbersMinimumOutput.Length> miniInputNeuros.Length/4) continue;
				for (int neu = 0 ; neu < hubNeuronNumbers.Length ; neu++ ) {
					int neuSumOut = 0;
					for (int i = 0 ; i < NetSize ; i++)
						if (this.ConnectivityMatrix[hubNeuronNumbers[neu],i]==true) neuSumOut++;
					if (minimumOutput==neuSumOut) ArrayManimulation.AddCell(ref hubNeuronNumbersMinimumOutput ,neu);
				}
			}
			
			
			// connect the hub group to thous neurons
			for (int neu = 0 ; neu < miniInputNeuros.Length ; neu++ ) {
				int flag=0,candidate=0;
				for (counter = 0 ; counter < Min_InterConnections ; counter++ ) {
					while (flag==0) {
						flag = 1;
						candidate = hubNeuronNumbersMinimumOutput[rnd.Next(0,hubNeuronNumbersMinimumOutput.Length)];
						if (this.ConnectivityMatrix[candidate,miniInputNeuros[neu]]==true) flag=0;
						if (candidate==miniInputNeuros[neu]) flag=0;
					}
					this.ConnectivityMatrix[candidate,miniInputNeuros[neu]]=true;
					TotalConnections++;
				}
			}
			//-----------------------------------------------------------------------------------------
			
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------

		
		public void Maass(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                  int NetSize,int[] posiORneg, ref globalParam Param)
		{
			//----------------- Maass et al Connection Method-------------------------
			int TotalConnection =0;
			while (TotalConnection < Param.Connections) {
				
				for (int xSource = 0 ; xSource < Param.Maass_column[0] ; xSource++) {
					for (int ySource = 0 ; ySource < Param.Maass_column[1] ; ySource++ ) {
						for (int zSource = 0 ; zSource < Param.Maass_column[2] ; zSource++ ) {
							
							int source = (xSource + (ySource*Param.Maass_column[0]) + (zSource*Param.Maass_column[0]*Param.Maass_column[1]) );
							int[] SourcePlace = new int[]{xSource,ySource,zSource};
							
							//------------Destination
							for (int xDest = 0 ; xDest < Param.Maass_column[0] ; xDest++ ) {
								for (int yDest = 0 ; yDest <Param.Maass_column[1] ; yDest++ ) {
									for (int zDest = 0 ; zDest < Param.Maass_column[2] ; zDest++ ) {
										
										int dest = (xDest + (yDest*Param.Maass_column[0]) + (zDest*Param.Maass_column[0]*Param.Maass_column[1]) );
										double C = 0;
										//Negative(2) OR Positive(1)
										if ((posiORneg[source]==1)&&(posiORneg[dest]==1)) C=0.3;
										if ((posiORneg[source]==1)&&(posiORneg[dest]==2)) C=0.2;
										if ((posiORneg[source]==2)&&(posiORneg[dest]==1)) C=0.4;
										if ((posiORneg[source]==2)&&(posiORneg[dest]==2)) C=0.1;
										
										double probability = this.EuclideanDistance(SourcePlace,new int[]{xDest,yDest,zDest});
										probability = C * Math.Exp(-Math.Pow((probability/Param.Maass_Lambda),2));
										if (probability>=1)
											Console.WriteLine("Error: wird");
										if (rnd.NextDouble()<=probability)
										{
											if (this.ConnectivityMatrix[source,dest]==true) continue;
											this.ConnectivityMatrix[source,dest] = true;
											TotalConnection++;
										}
									}
								}
							}//------------Destination
							
							
						}
					}
				}
			}
			
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		public double EuclideanDistance(int[] source,int[] destination){
			
			if (source.Length!=destination.Length) return 0;
			double distancce=0;
			for (int dimension = 0 ; dimension < source.Length ; dimension ++ ) {
				distancce+=Math.Pow((source[dimension]-destination[dimension]),2);
			}
			
			return Math.Sqrt(distancce);

		}//----------------------------------------------------------------------------------------------------------------------
		
		public void GroupsPowerLaw(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                           int NetSize,int[] posiORneg, ref globalParam Param){
			
			// stat creating Groups
			int[] Group_size = new int[0]{};
			int Group_size_Max=Param.Group_size_Max+1,
			Group_size_Min=Param.Group_size_Min,
			Group_sum=0;
			
			Matrix_Arithmetic Utils = new Matrix_Arithmetic();
			do{
				int candidateGroup = rnd.Next(Group_size_Min,Group_size_Max);
				if ((Group_sum+Group_size_Max+1)>=ConnectivityMatrix.GetLength(0))
					candidateGroup = (ConnectivityMatrix.GetLength(0) - Group_sum);
				Group_sum += candidateGroup;
				Utils.AddCell(ref Group_size,candidateGroup);
			}while (Group_sum<ConnectivityMatrix.GetLength(0));
			//-----------------------------------------------------------------------------------------
			Group_sum=Group_size.Length;
			int Group_counter=0;
			while(true){
				if (Group_sum/2==0) break;
				Group_sum=Group_sum/2;
				Group_counter++;
			}
			RandomPowerLaw[][] pwrlaw = new RandomPowerLaw[Group_counter][];
			int[][] shift = new int[Group_counter][];
			int[][] counter = new int[Group_counter][];
			
			for (int i = 0 ; i < Group_counter ; i++) {
				if (i==0) {
					pwrlaw[0] = new RandomPowerLaw[1];
					pwrlaw[0][0] = new RandomPowerLaw(NetSize , Param.IncreaseSelectedChanceBy);
					shift[0] = new int[1];
					shift[0][0] = 0;
					counter[0] = new int[1];
					counter[0][0] = 0;
				}else{
					pwrlaw[i] = new RandomPowerLaw[(int) Math.Pow(2,i)];
					shift[i] = new int[pwrlaw[i].Length];
					counter[i] = new int[pwrlaw[i].Length];
					
					int group_Start=0,group_end=0,groups=Group_size.Length/pwrlaw[i].Length,groupCounter=0;
					for (int t = 0 ; t < pwrlaw[i].Length ; t++ ) {
						for ( int g = groupCounter ; g < groups+groupCounter ; g++ ) { group_end += Group_size[g];	}
						groupCounter+=groups;
						pwrlaw[i][t] = new RandomPowerLaw( (group_end-group_Start), Param.IncreaseSelectedChanceBy);
						shift[i][t] = group_Start;
						group_Start=group_end;
					}
					if (groupCounter<Group_size.Length) {
						int sum=0;
						for (int t = 0 ; t < pwrlaw[i].Length-1 ; t++ ) {  sum += pwrlaw[i][t].nodes; }
						pwrlaw[i][pwrlaw[i].Length-1] = new RandomPowerLaw( NetSize - sum , Param.IncreaseSelectedChanceBy);
					}
				}
			}

			int TotalConnections=0;
			while( TotalConnections < Param.Connections){
				for (int level = 0 ; level < pwrlaw.Length ; level++ ) {
					for (int group = 0 ; group < pwrlaw[level].Length ; group++ ) {
						for (int nu = 0 ; nu < (int)  Math.Round(pwrlaw[level][group].nodes * Param.GeneralConnectivity) ; nu++ ) {
							int flag=0,tempCounter=0,candidate=0;
							pwrlaw[level][group].NewNode();
							int maxConection =(int)  Math.Round(pwrlaw[level][group].nodes * Param.GeneralConnectivity);
							int x = rnd.Next(0,pwrlaw[level][group].nodes);
							int LorR = (rnd.NextDouble()>0.5)? 1:0;
							while ((flag==0)&&(tempCounter<maxConection)) {
								flag=1;
								if (pwrlaw[level][group].Next(ref candidate)==false) break;
								tempCounter++;
								if (candidate==x) {
									pwrlaw[level][group].NotGood();
									flag=0;
								}else if ((this.ConnectivityMatrix[(x+shift[level][group]),(candidate+shift[level][group])]==true)&&(LorR==0)) {
									pwrlaw[level][group].NotGood();
									flag=0;
								}else if ((this.ConnectivityMatrix[(candidate+shift[level][group]),(x+shift[level][group])]==true)&&(LorR==1)) {
									pwrlaw[level][group].NotGood();
									flag=0;
								}
							}
							if (tempCounter<maxConection){
								if (LorR==0)
									this.ConnectivityMatrix[(x+shift[level][group]),(candidate+shift[level][group])]=true;
								else
									this.ConnectivityMatrix[(candidate+shift[level][group]),(x+shift[level][group])]=true;
								TotalConnections++;
								pwrlaw[level][group].Good();
							}
						}
					}
				}
			}
			
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		
		public void UncorrelatedScale_Free(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                                   int NetSize,int[] posiORneg, ref globalParam Param){
			// this function copied from Reservoir Computing Toolbox from RCToolbox-2.1.zip at http://snn.elis.ugent.be/rctoolbox
			// eric.antonelo [AT] UGent.be
			this.ConnectivityMatrix[0,1] = true;
			int TotalConnection =0;
			while (TotalConnection < Param.Connections) {
				int ski = 1;
				for (int i = 2 ; i < NetSize ; i++ ) {
					int tski = 0;
					for (int j = 0 ; j < i ;  j++ ) {
						int ki = 0;
						for (int count = 0 ; count < this.ConnectivityMatrix.GetLength(0) ; count++ ) {
							if (this.ConnectivityMatrix[j,count]==true) ki++;
							if (this.ConnectivityMatrix[count,j]==true) ki++;
						}
						if (rnd.NextDouble() < (ki / ski) ){
							if (rnd.NextDouble() > 0.5 )
								this.ConnectivityMatrix[j,i] = true ;
							else
								this.ConnectivityMatrix[i,j] = true ;
							TotalConnection++;
							tski++;
						}
					}
					ski += tski;
				}
			}
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		public void UncorrelatedScale_Free_Powerlaw(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                                            int NetSize,int[] posiORneg, ref globalParam Param){
			// this function copied from Reservoir Computing Toolbox from RCToolbox-2.1.zip at http://snn.elis.ugent.be/rctoolbox
			// eric.antonelo [AT] UGent.be
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		
		public void PowerLawSelections(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                               int NetSize,int[] posiORneg, ref globalParam Param){
			int TotalConnections=0;
			double decay = 0.03, diff=1;
			int[] connections = new int[NetSize];
			connections[0]= NetSize - 5;
			for (int i = 1 ; i < NetSize ; i++ ) {
				diff += (diff*decay);
				connections[i] =connections[i-1] - (int) Math.Round(diff);
				if (connections[i]<=2) connections[i]=2;
//				Console.WriteLine(connections);
			}
			RandomPowerLaw pwrlaw = new RandomPowerLaw(NetSize , Param.IncreaseSelectedChanceBy,connections);
			
			for ( int x=0 ; x < NetSize ; x++ ) {
				pwrlaw.NewNode();
				for (int times=0 ; times < connections[NetSize-1-x] ; times++) {
					int candidate = 0;
					pwrlaw.Next(ref candidate);
					while ((ConnectivityMatrix[x,candidate]==true) ||
					       (x==candidate)){
						pwrlaw.NotGood();
						pwrlaw.Next(ref candidate);
					}
					pwrlaw.Good();
					ConnectivityMatrix[x,candidate]=true;
					TotalConnections++;
				}
			}
//			Console.WriteLine(TotalConnections+" "+Param.Connections);
			//-----------------------------------------------------------------------------------------
//			PrintNetwork();
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		public void TwoWayPowerLaw(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                           int NetSize,int[] posiORneg, ref globalParam Param){
			int TotalConnections = 0;
			// stat the minimul Group connections
			int[] Group_size = new int[0]{};
			int Group_size_Max=Param.Group_size_Max+1,
			Group_size_Min=Param.Group_size_Min,
			Group_sum=0,
			Min_InterConnections = Param.Group_interconnections[0],
			Max_InterConnections = 0;

			Matrix_Arithmetic Utils = new Matrix_Arithmetic();
			do{
				int candidateGroup = rnd.Next(Group_size_Min,Group_size_Max);
				if ((Group_sum+Group_size_Max+1)>=ConnectivityMatrix.GetLength(0))
					candidateGroup = (ConnectivityMatrix.GetLength(0) - Group_sum);
				Group_sum += candidateGroup;
				Utils.AddCell(ref Group_size,candidateGroup);
			}while (Group_sum<ConnectivityMatrix.GetLength(0));

			int endGroup = Group_size[0]-1,startGroup=0,counter = 0 ;
			for (int x = 0 ; x < ConnectivityMatrix.GetLength(0) ; x++ ) {
				if ((x == endGroup) && (counter+1<Group_size.Length)) {
					counter++;
					startGroup = endGroup;
					endGroup += Group_size[counter];
				}
				int[] candidates;
				if (Param.Group_interconnections.Length==2){
					if (Param.Group_interconnections[1]>=Group_size[counter])
						Max_InterConnections = Group_size[counter]-1;
					else
						Max_InterConnections = Param.Group_interconnections[1];
				}else
					Max_InterConnections = Group_size[counter]-1;
				if (Max_InterConnections < Min_InterConnections)
					candidates = new int[rnd.Next(Max_InterConnections,Min_InterConnections+1)];
				else
					candidates = new int[rnd.Next(Min_InterConnections,Max_InterConnections+1)];
				rndA.dselect(startGroup,endGroup,x,ref candidates);
				for (int i = 0 ; i < candidates.Length ; i++ ) {
					this.ConnectivityMatrix[x,candidates[i]]=true;
					TotalConnections++;
				}
			}
			//-----------------------------------------------------------------------------------------
			int[] connections = new int[NetSize];
			connections[NetSize-1]  = NetSize-1;
			double decay = 0.03, diff=1;
			int tempConnection = TotalConnections+connections[NetSize-1],
			minimumConnection = 3;
			for (int i = NetSize-2 ; i >= 0 ; i-- ) {
				diff += (diff*decay);
				connections[i] = connections[i+1] - (int) Math.Round(diff);
				tempConnection+=connections[i];
				if (connections[i]<=minimumConnection)
					if (tempConnection>=Param.Connections)
						connections[i]=1;
					else
						connections[i]=minimumConnection;
			}
			
			for ( int x=0 ; x < NetSize ; x++ ) {
				int y = NetSize-1;
				for (int times=0 ; times < connections[NetSize-x-1] ; times++) {
					if ((x!=y)&&(ConnectivityMatrix[y,x]==false)&&(rnd.NextDouble()>=0)){
						ConnectivityMatrix[y,x]=true;
						TotalConnections++;
					}
					y--;
				}
			}
			
			//-----------------------------------------------------------------------------------------
//			PrintNetwork();
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		
		public void TwoWayLinearDescent(ref double[,] weight,ref int[,] delay,ref bool[,] ConnectivityMatrix,
		                                int NetSize,int[] posiORneg, ref globalParam Param){
			int TotalConnections = 0;
			int parts = 1 ;
			int numOfNodes = (NetSize/parts);
			// round the parts to avoid half groups
			parts = NetSize/numOfNodes;
			
			// conect groups of neurons internally
			for (int part = 0 ; part < parts ; part++ ) {
				for (int x= part*numOfNodes+1 ; x < (part+1)*numOfNodes ; x++ ) {
					for (int i=part*numOfNodes; i < x ; i++) {
						ConnectivityMatrix[x,i]=true;
						TotalConnections++;
					}
					
				}
			}
			// conect the Zero neurons internally to his group
			for (int part = 0 ; part < parts ; part++ ) {
				int x= part*numOfNodes;
				ConnectivityMatrix[x,rnd.Next(x+1,((part+1)*numOfNodes))]=true;
				TotalConnections++;
			}
			
			// connect individual neuron to other groups
			int connectionPerNeuron = Param.Connections - TotalConnections;
			if (connectionPerNeuron>0){
				connectionPerNeuron = connectionPerNeuron/NetSize;
				long tempSeed = (System.DateTime.UtcNow.Ticks);
				rndA = new randomIntArr(Convert.ToInt32(tempSeed%int.MaxValue));
				int[] connections = new int[connectionPerNeuron];
				
				for (int part = 0 ; part < parts ; part++ ) {
					int[] dont = new int[numOfNodes];
					for(int i = 0 ; i < numOfNodes ; i++)
						dont[i] = part*numOfNodes+i;
					for (int x= part*numOfNodes ; x < (part+1)*numOfNodes ; x++ ) {
						rndA.dselect(0,NetSize,dont,ref connections);
						for (int i = 0 ; i < connections.Length ; i++ ) {
							ConnectivityMatrix[x,connections[i]]=true;
							TotalConnections++;
						}
					}
				}
			}
			//-----------------------------------------------------------------------------------------
//			PrintNetwork();
			finishNetworkRewiring(ref weight,ref delay,ref ConnectivityMatrix,NetSize,posiORneg,ref Param);
		}//----------------------------------------------------------------------------------------------------------------------
		
		
	} // End Class
}
